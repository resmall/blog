### Como fazer deploy da aplicação

Rodar os seguintes comandos:

bundle install

Isso irá instalar as dependências do projeto. 

Depois disso, será necessário usar o seguinte comando para testar a aplicação através do Vagrant:

rails s -b 0.0.0.0

Isso fará com que seja utilizado o driver do vagrant, permitindo acessar pelo localhost da máquina hospedeira da VM.
Ex:
localhost:3000 deverá acessar a aplicação.